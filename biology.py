from json import *

#Question 1
def est_base(str):
    """
    vérifier si un caractère est un ADN
    :param str: un caractère
    :return:bool
    """
    return str=="A" or str=="T" or str=="G" or str=="C"

print(est_base("A"))

#Question 2
def est_adn(chaine):
    """
    vérifier si une chaîne correspond à un ADN (est constituée uniquement des caractères A, T, G, C)
    :param chaine:
    :return: bool
    """
    i = 0
    while i < len(chaine) and (chaine[i]=="A" or chaine[i]=="T" or chaine[i]=="G" or chaine[i]=="C"):
        i+=1
    return i == len(chaine)

print(est_adn("ATCAAA"))

#Question 3
def arn(str):
    """
    L'ARN est construit à partir de l'ADN en remplaçant la thymine T par l'uracile codé par la lettre U
    :param str: une séquence d'ADN
    :return: une séquence ARN
    """
    if est_adn(str) == False:
        print("la chîne de caractère n'est pas un ADN")
    else:
        valeur = str.replace("T","U")
        return valeur
print(arn("ATGTCAAA"))

#Question 4
def arn_to_codons(ch):
    """
    :param ch: une chaîne de caractères correspondant à de l'ARN
    :return: un tableau contenant la liste des codons
    """
    tab_codons = []
    i = 0
    while i < len(ch) and len(ch) % 3 == 0 :
        tab_codons.append(ch[i:i+3])
        i+=3
    j = 0
    while j < len(ch) and len(ch) % 3 != 0:
        tab_codons.append(ch[j:j+3])
        j+=3
    if j > len(ch):
        tab_codons.pop()
    return tab_codons

print(arn_to_codons("CGUUAGGGG"))

# Question 5
def load_dico_codons_aa(file):
    """
    :param file: un fichier au format JSON
    :return: la structure de données chargée en mémoire à partir du JSON.
    """
    fichier = open(file,"r")
    strjson = fichier.read()
    fichier.close()
    codons = loads(strjson)
    return codons

dico_codons_acidesamines =load_dico_codons_aa("codons_aa.json")
print(dico_codons_acidesamines)


def codons_stop(dico):
    """
    :param dico: un dictionnaire dont les clés sont les codons et les valeurs les acides aminés
    :return:un tableau contenant l'ensemble des codons stop
    """
    codonsstop = []
    codons = list(dico)
    for i in codons:
        if i == "UGA" or i=="UAA" or i=="UAG":
            codonsstop.append(i)

    return codonsstop
print(codons_stop(dico_codons_acidesamines))


# Q6
def codons_to_aa(tab,dico):
    """

    :param tab: un tableau de codons
    :param dico: le dictionnaire de correspondance entre codons et acides aminés
    :return: La fonction devra retourner un tableau contenant les acides aminés correspondant aux codons
    """
    tab_acides_amines = []
    i = 0
    while i < len(tab) and tab[i]!="UGA" and tab[i]!="UAA" and tab[i]!="UAG":
        codon = tab[i]
        acide = dico[codon]
        tab_acides_amines.append(acide)
        i+=1
    return tab_acides_amines

print(codons_to_aa(["CGU", "AAU", "UAA", "GGG", "CGU"],dico_codons_acidesamines))




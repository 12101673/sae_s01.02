# question 1
def test_est_base():
    assert est_base("A")
    assert est_base("T")
    assert est_base("G")
    assert est_base("C")
    assert not est_base("B")
    assert not est_base("D")
    assert not est_base("Z")
    assert not est_base("z")
    assert not est_base("i")
    assert not est_base("a")
    assert not est_base("t")
    assert not est_base("1")
    assert not est_base("2")
    print("test de la fonction est_base est : ok")
test_est_base()
# question 2
def test_est_adn():
    assert est_adn("ATGC")
    assert not est_adn("atgc")
    assert est_adn("ATTGGCCA")
    assert not est_adn("ABFUIVDBN")
    assert est_adn("AACGTGCAT")
    assert not est_adn("guebvfni")
    assert not est_adn("FZHBD VHD")
    assert not est_adn("ATGC ATGC")
    print("test de la fonction est_adn est : ok")
test_est_adn()
# question 3
def test_arn():
    assert arn("ATGC")=="AUGC"
    assert arn("ATTCCA")=="AUUCCA"
    assert arn("TTTTTT")=="UUUUUU"
    assert arn("AGCTCATT")=="AGCUCAUU"
    print("test de la fonction arn est : ok")
test_arn()

# question 4
def test_arn_to_codons():
    assert arn_to_codons("CGUUAGGGG")==["CGU","UAG","GGG"]
    assert arn_to_codons("UUGCUAA")==["UUG","CUA"]
    assert arn_to_codons("UUGCUAAG")==["UUG","CUA"]
    assert arn_to_codons("UUGCUAAGA")==["UUG","CUA","AGA"]
    print("test de la fonction arn_to_codons est : ok ")
test_arn_to_codons()
